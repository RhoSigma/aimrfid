#ifndef pins_h
    #define pins_h 1

    #include <Arduino.h>

    constexpr uint8_t LED_PIN = LED_BUILTIN;     // Configurable, see typical pin layout above

    constexpr uint8_t RST_PIN = 5;     // Configurable, see typical pin layout above
    constexpr uint8_t SS_PIN = 4;     // Configurable, see typical pin layout above
#endif
